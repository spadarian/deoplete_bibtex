import os
import re
import yaml

import bibtexparser
from bibtexparser.bparser import BibTexParser

from deoplete.base.source import Base
from deoplete.util import getlines


class Source(Base):
    def __init__(self, vim):
        super().__init__(vim)

        self.name = 'deoplete_bibtex'
        self.filetypes = ['tex', 'markdown']
        self.mark = '[ref]'
        self.min_pattern_length = 0
        self.rank = 9999
        self.dup = False
        self.vim = vim

        self.matchers = ['matcher_length', 'matcher_contains']
        # self.sorters = ['sorter_smart']
        self.matcher_key = 'info'

        patt = r'\\(C|c)ite\w*\*{,1}(\[(.*?)\]){,1}\{\w*,*$|@$'
        self.__pattern = re.compile(patt)
        self.parser_kwargs = {}
        file_bibs = self.__find_bib_in_file()
        self.__bib_paths = file_bibs
        self.__bib_entries = {}
        self.__candidates = []
        self.mtime = {}

        self.__read_bib_files()
        self.__cache_candidates()

    @property
    def verbose(self):
        if 'deoplete_bibtex_verbose' in self.vim.vars:
            if self.vim.eval('g:deoplete_bibtex_verbose'):
                return True
        return False

    @staticmethod
    def message(msg):
        print('[deoplete-bibtex]', msg)

    def __find_bib_in_file(self):
        ft = self.get_buf_option('filetype')
        paths = []
        if 'tex' in ft:
            paths = self.__find_bib_in_file_tex()
        elif 'markdown' in ft:
            paths = self.__find_bib_in_file_md()
        return paths

    def __find_bib_in_file_tex(self):
        wd = self.vim.call('expand', '%:p:h')
        bib = re.compile('^\\\\bibliography{')
        lines = [line for line in getlines(self.vim) if bib.search(line)]
        if not lines:
            return []
        paths = re.findall(r'(?<={).*(?=})', lines[0])
        if paths:
            paths = paths[0].split(',')
            paths = [os.path.abspath(os.path.join(wd, p)) for p in paths]
            paths = [f'{p}.bib' for p in paths]
            return paths

    def __find_bib_in_file_md(self):
        wd = self.vim.call('expand', '%:p:h')
        if '---' in getlines(self.vim, 1, 1):
            header = []
            for line in getlines(self.vim, 2):
                if '---' in line:
                    break
                else:
                    header.append(line)
            header = yaml.safe_load('\n'.join(header))
            if 'bibliography' in header:
                paths = header['bibliography']
                if not isinstance(paths, list):
                    paths = [paths]
                paths = [os.path.abspath(os.path.join(wd, p)) for p in paths]
                return paths
        return []

    def gather_candidates(self, context):
        result = self.__pattern.search(context['input'])
        if result is not None:
            if not self.__candidates:
                self.__cache_candidates()
            return self.__candidates

    @staticmethod
    def __format_info(d, path):
        title = d.get('title', '')
        title = title.replace('{', '')
        title = title.replace('}', '')
        authors = d.get('author', '')
        year = d.get('year', '')
        lines = [
            f'Title: {title}',
            f'Authors: {authors}',
            f'Year: {year}',
            f'File: {os.path.basename(path)}',
        ]
        return '\n'.join(lines)

    def __format_candidate(self, k, d, path):
        return {
                'word': k,
                'info': self.__format_info(d, path),
                }

    def __cache_candidates(self):
        all_candidates = []
        for path, entries in self.__bib_entries.items():
            all_candidates.extend([self.__format_candidate(k, v, path)
                                   for k, v in entries.items()])
        self.__candidates = all_candidates

    def __clear_bib(self):
        self.__bib_entries = {}

    @staticmethod
    def __is_valid_file(path):
        return os.path.isfile(path) and os.path.getsize(path) > 0

    def __update_mtime(self, path):
        self.mtime.update({path: os.stat(path).st_mtime})

    def __read_changed(self):
        for path in self.__bib_paths:
            old = self.mtime.get(path)
            if os.path.exists(path):
                current = os.stat(path).st_mtime
                if current != old:
                    self.__bib_entries[path] = self.__read_single_file(path)
            else:
                self.print_error(
                    f'[deoplete-bibtex] No such file: {path}'
                )
        # Remove files if not used
        for path in (set(self.mtime.keys()) - set(self.__bib_paths)):
            self.__bib_entries.pop(path)
            self.mtime.pop(path)
        self.__cache_candidates()

    def __read_single_file(self, path):
        try:
            if not self.__is_valid_file(path):
                return {}
            with open(path) as bf:
                parser = BibTexParser(**self.parser_kwargs)
                bib = bibtexparser.load(bf, parser=parser).entries_dict
                if self.verbose:
                    self.message(f'Loading entries: {path}')
                self.__update_mtime(path)
                return bib
        except FileNotFoundError:
            self.print_error(
                f'[deoplete-bibtex] No such file: {path}\n'
            )
        except Exception:
            self.print_error(
                '[deoplete-bibtex] Unknown error\n'
            )

    def __read_bib_files(self):
        self.__clear_bib()
        for path in self.__bib_paths:
            bib = self.__read_single_file(path)
            self.__bib_entries.update({path: bib})

    def on_event(self, context):
        file_bibs = self.__find_bib_in_file()
        self.__bib_paths = file_bibs
        self.__read_changed()

